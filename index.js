let numA = parseInt(prompt("Provide a number"));
let numB = parseInt(prompt("Provide another number"));
let total = numA + numB
console.log(total);

if (total >= 30) {
    alert("The quotient of the two numbers are: " + (numB / numA));
} else if (total >= 20) {
    alert("The product of the two numbers are: " + (numB * numA));
} else if (total >= 10){
    alert("The difference of the two numbers are: " + (numB - numA));
} else {
    console.warn("The sum of the two numbers are: " + total);
}

let name = prompt("What is your name?");
let age = parseInt(prompt("What is your age?"));

if (name === "" || name === null || age > 100 || age === null) {
    alert("Are you a time traveler?");
} else if (name !== "" && age !== ""){
    alert("Hello " + name + ". Your age is " + age);
}

function isLegalAge(age) {
    age >= 18 ? alert("You are of legal age.") : alert("You are not allowed here.");
}

isLegalAge(age);

switch (age) {
    case 18:
        alert("You are now allowed to party.")
        break;
    case 21:
        alert("You are now part of the adult society.")
        break;
    case 65:
        alert("We thank you for your contribution to society.")
        break;
    default:
        alert("Are you sure you're not an alien?");
        break;
}

try {
    isLegalag(age);
} catch (error) {
    console.warn(error.message);
} finally {
    isLegalAge(age);
}
